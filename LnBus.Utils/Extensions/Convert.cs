﻿using System;
using System.Globalization;

namespace LnBus.Utils.Extensions
{
    public static class Convert
    {
        /// <summary>
        /// Parsea un string a un formato de fecha utilizando InvariantCulture.
        /// </summary>
        /// <param name="str">String a parsear.</param>
        /// <param name="format">Formato para el parseo.</param>
        /// <returns></returns>
        public static DateTime? ToDateTime(this string str, string format) 
        {
            try
            {
                return DateTime.ParseExact(str, format, CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
