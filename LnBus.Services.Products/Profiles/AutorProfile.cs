﻿using AutoMapper;
using LnBus.BusinessEntities.SqlServer.Sample;
using LnBus.Services.Products.DTOs;

namespace LnBus.Services.Products.Profiles
{
    public class AutorProfile : Profile
    {
        public AutorProfile()
        {
            CreateMap<Autor, AutorDTO>();
        }
    }
}
