﻿namespace LnBus.Services.Products.DTOs
{
    public class AutorDTO
    {
        public string Nombres { get; set; }
        public string Apellido { get; set; }
    }
}
