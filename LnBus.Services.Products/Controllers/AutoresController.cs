﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using LnBus.BLL.Products;
using LnBus.BusinessEntities.SqlServer.Sample;
using LnBus.Contracts.Repositories;
using LnBus.Services.Products.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace LnBus.Services.Products.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AutoresController : ControllerBase
    {
        private readonly IRepository<Autor> _repository;
        private readonly IMapper _mapper;
        private readonly ProductService _productService;

        public AutoresController(IRepository<Autor> repository, IMapper mapper, ProductService productService)
        {
            _repository = repository;
            _mapper = mapper;
            _productService = productService;
        }

        [HttpGet]
        public IEnumerable<AutorDTO> Get()
        {
            var autors = _repository.Find();
            var result = _mapper.Map<IEnumerable<AutorDTO>>(autors);
            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var autor = await _productService.FindAutor(id);
            if (autor == null) 
            {
                return NotFound();
            }
            var result = _mapper.Map<AutorDTO>(autor);
            return Ok(result);
        }
    }
}
