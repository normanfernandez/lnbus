﻿using LnBus.BusinessEntities.SqlServer.Sample;
using LnBus.Repositories.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LnBus.BLL.Products
{
    public class ProductService
    {
        private readonly DbContext _dbContext;

        public ProductService(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Autor> GetAutors() 
        {
            return _dbContext.Set<Autor>().AsNoTracking();
        }

        public async Task<Autor> FindAutor(int id) 
        {
            return await _dbContext.Set<Autor>().AsQueryable().FirstOrDefaultAsync(c => c.Id == id);
        }
    }
}
