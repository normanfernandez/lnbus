﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LnBus.Contracts.Repositories
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Find();
        T Find(Expression<Func<T, bool>> exp);
        Task Save(T entity);
        Task Delete(T entity);
    }
}
