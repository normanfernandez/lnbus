﻿using LnBus.BusinessEntities.SqlServer.Sample;

namespace LnBus.Contracts.Repositories
{
    public interface IAutorRepository : IRepository<Autor>
    {
        
    }
}
