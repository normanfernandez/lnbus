﻿using System;

namespace LnBus.BusinessEntities.Oracle.Repo
{
    public class MG_REPO_CONOZCA_CLIENTE
    {
        public string CEDULA { get; set; }
        public string NOMBRES { get; set; }
        public string PRIMER_APELLIDO { get; set; }
        public string SEGUNDO_APELLIDO { get; set; }
        public DateTime? FECHA_ADICION { get; set; }
        public DateTime? FECHA_MODIFICACION { get; set; }
    }
}
