﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LnBus.BusinessEntities.Oracle.Abanks
{
    public class MG_CLIENTES
    {
        public int CODIGO_CLIENTE { get; set; }
        public int CODIGO_TIPO_IDENTIFICACION { get; set; }
        public string NUMERO_IDENTIFICACION { get; set; }
        public string NOMBRES { get; set; }
        public string PRIMER_APELLIDO { get; set; }
        public string SEGUNDO_APELLIDO { get; set; }
    }
}
