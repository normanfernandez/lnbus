﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;

namespace LnBus.BusinessEntities.Oracle.Abanks.Mappings
{
    public class MG_CLIENTESMapping : EntityTypeConfiguration<MG_CLIENTES>
    {
        public MG_CLIENTESMapping()
        {
            ToTable("MG_CLIENTES");

            HasKey(p => p.CODIGO_CLIENTE);

            Property(p => p.CODIGO_CLIENTE).HasColumnName("CODIGO_CLIENTE");
        }
    }
}
