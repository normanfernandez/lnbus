﻿using System;

namespace LnBus.BusinessEntities.SqlServer.Sample
{
    public class Autor
    {
        public int Id { get; set; }
        public string Nombres { get; set; }
        public string Apellido { get; set; }
        public DateTime? Fecha_reg { get; set; }
    }
}
