﻿using LnBus.Contracts.Repositories;
using LnBus.Repositories.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LnBus.Repositories
{
    public class SqlServerRepository<T> : IRepository<T> where T :class
    {

        private readonly DbContext _dbContext;

        public SqlServerRepository(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Find()
        {
            return _dbContext.Set<T>().AsNoTracking();
        }

        public T Find(Expression<Func<T, bool>> exp)
        {
            return _dbContext.Set<T>().FirstOrDefault(exp);
        }

        public Task Save(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
