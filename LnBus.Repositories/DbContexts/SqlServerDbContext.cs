﻿using LnBus.BusinessEntities.SqlServer.Sample;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LnBus.Repositories.DbContexts
{
    public class SqlServerDbContext: DbContext
    {
        public SqlServerDbContext(DbContextOptions<SqlServerDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Autor> Autores { get; set; }
    }
}
