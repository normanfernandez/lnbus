﻿using LnBus.BusinessEntities.Oracle.Abanks;
using Microsoft.EntityFrameworkCore;

namespace LnBus.Repositories
{
    public class OracleDbContext : DbContext
    {
        public OracleDbContext(): base()
        {

        }

        public DbSet<MG_CLIENTES> MG_CLIENTES { get; set; }
    }
}
